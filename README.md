# luxarg :


LUXARG is another keyboard friendly text editor.

This project started at August 2019 and restarted at 2021 TODAY!

Easy to use and user-friendly !




LUXARG support :

Fedora, CENTOS and RedHat,

Debian and Ubuntu,

OpenSUSE,

Arch and Manjaro, ...


# ICON

![ICON](icon/luxarg.png)


# screenshot :

![screenshot](screenshot/1.png)


# Licensing

AMZY-0 (M.Amin Azimi .K) 

Copyright (C) (2019-2020-2021)  AMZY-0 (M.Amin Azimi .K) 

"Luxarg" (This program) is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.



# KEYS : 

    INSERT MODE : <F1>
    SAVE   MODE : <F2>
    OPEN   MODE : <F3>
    HELP   MODE : <F4>
    DELETE ALL  : <Ctrl + 0>
    SELECT ALL  : <Ctrl + />
    CORSUR RIGHT: <Ctrl + f> The CURSOR move the cursor forward one space.
    CORSUR LEFT : <Ctrl + b> The CURSOR move the cursor backward one space.
    Copy        : <Ctrl + c>
    Paste       : <Ctrl + v>
    Cut         : <Ctrl + w>
    UNDO        : <Ctrl + z>
    REDO        : <Ctrl + r>
    HELP   CLI  : luxarg <-h/--help>
    ZOOM IN     : <Ctrl + sroll UP>
    ZOOM OUT    : <Ctrl + sroll Down>

# INSTALLATION
    $ python<3X> installer.py

# dependencies
    $ pip3 install -r requirements.txt

# update method
	    
    old method(v0.1, v0.1.1){
		
                $ cd ~/.luxarg
    		
                $ python<3X> update.py
    	}


    $ luxarg-update (v0.1.2)


# Install pip
    pip.txt

